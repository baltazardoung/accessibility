## GitLab Accessiblity Changelog

GitLab Accessibility versions follow the Pa11y versions used, and generate a <PA11Y_VERSION>-(gitlab.<CHANGE_INCREMENT>)? Docker image tag.

### master (unreleased)

- Add and fix shebang lines on shell scripts

### 5.3.0-gitlab.3

- Rename Pa11y JSON artifact `accessibility.json` => `gl-accessibility.json`

### 5.3.0-gitlab.2

- Use absolute paths to prevent missing configs in different execution directories

### 5.3.0-gitlab.1

- Added support for producting HTML artifacts for multiple URLs
- Added single artifact file for JSON reports

### 5.3.0

This marks the start of the Gitlab Accessibility Docker image. It comes with two libraries intended for CI use:

- pa11y v5.3.0
- pa11y-ci v2.3.0
